<?php

namespace Tests;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Laravel\Dusk\TestCase as BaseTestCase;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)
//            ->setBinary($this->getChromeBinaryPath())
            ->addArguments([
                '--disable-gpu',
                '--headless',
                '--window-size=1920,1080',
//                '--no-sandbox'
            ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

    protected function getChromeBinaryPath()
    {
        exec('cd ' . base_path() . ' && node chrome-path.js', $out, $err);

        if ($err) {
            throw new \Exception('Could not find path to chrome binary');
        }

        return $out[0];
    }
}
